This is a custom written CKAN extension that adds custom fields to capture
additional metadata fields to comply with the US Project Open Data standards 
found at https://project-open-data.cio.gov/v1.1/schema/
