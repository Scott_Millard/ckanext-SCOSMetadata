# encoding: utf-8

import logging

import ckan.plugins as plugins
import ckan.plugins.toolkit as tk

class SCOSMetadataPlugin(plugins.SingletonPlugin,
        tk.DefaultDatasetForm):
   
    # this is based on 
    #http://docs.ckan.org/en/latest/extensions/adding-custom-fields.html
    
    #validations need to be added see:
    # http://docs.ckan.org/en/latest/extensions/adding-custom-fields.html#custom-validators
    
    plugins.implements(plugins.IConfigurer, inherit=False)
    plugins.implements(plugins.IDatasetForm, inherit=False)
    plugins.implements(plugins.ITemplateHelpers, inherit=False)

    def get_helpers(self):
        return []

    def update_config(self, config):
        # Add this plugin's templates dir to CKAN's extra_template_paths, so
        # that CKAN will use this plugin's custom templates.
        tk.add_template_directory(config, 'templates')

    def is_fallback(self):
        # Return True to register this plugin as the default handler for
        # package types not handled by any other IDatasetForm plugin.
        return True

    def package_types(self):
        # This plugin doesn't handle any special package types, it just
        # registers itself as the default (above).
        return []

    def _modify_package_schema(self, schema):
        # Add custom metadata fields to the schema
        schema.update({
                'accessLevel': [tk.get_validator('ignore_missing'),
                    tk.get_converter('convert_to_extras')],
                'rights': [tk.get_validator('ignore_missing'),
                    tk.get_converter('convert_to_extras')],
                'spatial': [tk.get_validator('ignore_missing'),
                    tk.get_converter('convert_to_extras')],
                'temporal': [tk.get_validator('ignore_missing'),
                    tk.get_converter('convert_to_extras')],
                'issued': [tk.get_validator('ignore_missing'),
                    tk.get_converter('convert_to_extras')],
                'accrualPeriodicity': [tk.get_validator('ignore_missing'),
                    tk.get_converter('convert_to_extras')],
                'conformsTo': [tk.get_validator('ignore_missing'),
                    tk.get_converter('convert_to_extras')],
                'describedBy': [tk.get_validator('ignore_missing'),
                    tk.get_converter('convert_to_extras')],
                'describedByType': [tk.get_validator('ignore_missing'),
                    tk.get_converter('convert_to_extras')],
                'isPartOf': [tk.get_validator('ignore_missing'),
                    tk.get_converter('convert_to_extras')],
                'language': [tk.get_validator('ignore_missing'),
                    tk.get_converter('convert_to_extras')],
                'landingPage': [tk.get_validator('ignore_missing'),
                    tk.get_converter('convert_to_extras')],
                'references': [tk.get_validator('ignore_missing'),
                    tk.get_converter('convert_to_extras')]
                })
        return schema

    def create_package_schema(self):
        schema = super(SCOSMetadataPlugin, self).create_package_schema()
        schema = self._modify_package_schema(schema)
        return schema

    def update_package_schema(self):
        schema = super(SCOSMetadataPlugin, self).update_package_schema()
        schema = self._modify_package_schema(schema)
        return schema

    def show_package_schema(self):
        schema = super(SCOSMetadataPlugin, self).show_package_schema()

        # Don't show vocab tags mixed in with normal 'free' tags
        # (e.g. on dataset pages, or on the search page)
        schema['tags']['__extras'].append(tk.get_converter('free_tags_only'))

        # Add our custom metadata fields to the dataset schema.
        schema.update({
            'accessLevel': [tk.get_converter('convert_from_extras'),
                tk.get_validator('ignore_missing')],
            'rights': [tk.get_converter('convert_from_extras'),
                tk.get_validator('ignore_missing')],
            'spatial': [tk.get_converter('convert_from_extras'),
                tk.get_validator('ignore_missing')],
            'temporal': [tk.get_converter('convert_from_extras'),
                tk.get_validator('ignore_missing')],
            'issued': [tk.get_converter('convert_from_extras'),
                tk.get_validator('ignore_missing')],
            'accrualPeriodicity': [tk.get_converter('convert_from_extras'),
                tk.get_validator('ignore_missing')],
            'conformsTo': [tk.get_converter('convert_from_extras'),
                tk.get_validator('ignore_missing')],
            'describedBy': [tk.get_converter('convert_from_extras'),
                tk.get_validator('ignore_missing')],
            'describedByType': [tk.get_converter('convert_from_extras'),
                tk.get_validator('ignore_missing')],
            'isPartOf': [tk.get_converter('convert_from_extras'),
                tk.get_validator('ignore_missing')],
            'language': [tk.get_converter('convert_from_extras'),
                tk.get_validator('ignore_missing')],
            'landingPage': [tk.get_converter('convert_from_extras'),
                tk.get_validator('ignore_missing')],
            'references': [tk.get_converter('convert_from_extras'),
                tk.get_validator('ignore_missing')],
            })
        return schema

    def setup_template_variables(self, context, data_dict):
        return super(SCOSMetadataPlugin, self).setup_template_variables(
                context, data_dict)

    def new_template(self):
        return super(SCOSMetadataPlugin, self).new_template()

    def read_template(self):
        return super(SCOSMetadataPlugin, self).read_template()

    def edit_template(self):
        return super(SCOSMetadataPlugin, self).edit_template()

    def search_template(self):
        return super(SCOSMetadataPlugin, self).search_template()

    def history_template(self):
        return super(SCOSMetadataPlugin, self).history_template()

    def package_form(self):
        return super(SCOSMetadataPlugin, self).package_form()
