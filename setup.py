from setuptools import setup, find_packages

version = '0.2.1'

setup(
	name='ckanext-scosmetadata',
	version=version,
	description='SCOS Metadata Plugin',
	long_description='',
	classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
	keywords='',
	author='',
	author_email='',
	url='',
	license='',
	packages=find_packages(),
	namespace_packages=['ckanext', 'ckanext.SCOSMetadata'],
	include_package_data=True,
	zip_safe=False,
	install_requires=[],
	entry_points=\
	"""
        [ckan.plugins]
	    SCOSMetadata=ckanext.SCOSMetadata.plugin:SCOSMetadataPlugin
	""",
)
